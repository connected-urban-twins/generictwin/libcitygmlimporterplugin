/**

The MIT License (MIT)

Copyright (c) 2023 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/

#pragma once

#include "ICityGMLImporter.h"
#include "CityGMLImporterTypes.h"

#include <memory>
#include <string>
#include <set>

namespace citygml
{
class CityModel;
class CityObject;
class Geometry;

}

DECLARE_LOG_CATEGORY_EXTERN(LogLibCityGMLImporter, Log, All);

/**
 * 
 */
class FLibCityGMLImporter	:	public GenericTwin::ICityGMLImporter
{
public:

	static GenericTwin::ICityGMLImporter* Create();

	FLibCityGMLImporter();
	virtual ~FLibCityGMLImporter();

	virtual FString GetImporterName() const override;

	virtual bool Load(const FString &cityGMLFile) override;

	virtual TArray<FString> GetThemes() const override;

	const FVector& GetMinExtent() const override;
	const FVector& GetMaxExtent() const override;

	virtual TArray<GenericTwin::SCityGMLBuilding> GetBuildings(GenericTwin::ICityGMLCoordinateTransform &transform, const FString &Theme, const TSet<FString> &filterList = TSet<FString>(), bool isWhiteList = true)  override;

private:

	bool createBuilding(GenericTwin::ICityGMLCoordinateTransform &transform, const citygml::CityObject &cityObj, const char *theme, GenericTwin::SCityGMLBuilding &building);

	void addSurface(GenericTwin::ICityGMLCoordinateTransform &transform, const citygml::Geometry &geometry, const char *theme, GenericTwin::SCityGMLBuilding &building);
	void addSurfaces(GenericTwin::ICityGMLCoordinateTransform &transform, const citygml::Geometry &geometry, const char *theme, GenericTwin::SCityGMLBuilding &building);

	bool isIncluded(const FString &item, const TSet<FString> &list, bool include) const;

	GenericTwin::SCityGMLSurface::Type getSurfaceType(const citygml::Geometry &geometry);

	std::shared_ptr<const citygml::CityModel>		m_CityModel;

	FVector											m_MinExtent;
	FVector											m_MaxExtent;

};


inline const FVector& FLibCityGMLImporter::GetMinExtent() const
{
	return m_MinExtent;
}

inline const FVector& FLibCityGMLImporter::GetMaxExtent() const
{
	return m_MaxExtent;
}

inline bool FLibCityGMLImporter::isIncluded(const FString &item, const TSet<FString> &list, bool isWhiteList) const
{
	return list.IsEmpty() ? true : (list.Contains(item) ? isWhiteList : !isWhiteList);
}
