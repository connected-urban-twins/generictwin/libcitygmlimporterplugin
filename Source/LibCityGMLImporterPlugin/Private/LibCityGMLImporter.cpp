/**

The MIT License (MIT)

Copyright (c) 2023 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/

#include "LibCityGMLImporter.h"

#include "ICityGMLCoordinateTransform.h"

#include <citygml/citygml.h>
#include <citygml/citymodel.h>
#include <citygml/geometry.h>
#include <citygml/polygon.h>
#include <citygml/implictgeometry.h>
#include <citygml/texture.h>

#include <citygml/citygmllogger.h>

DEFINE_LOG_CATEGORY(LogLibCityGMLImporter);

class Logger : public citygml::CityGMLLogger
{
public:

	Logger()
		: CityGMLLogger(citygml::CityGMLLogger::LOGLEVEL::LL_ERROR)
		, LogLevelNames({ FString(TEXT("Trace")) , FString(TEXT("Debug")) , FString(TEXT("Info")) , FString(TEXT("Warning")), FString(TEXT("Error")) })
	{	}

	virtual ~Logger()
	{
	}

	virtual void log(LOGLEVEL level, const std::string& message, const char* file = nullptr, int line = -1) const
	{
		FString msg(message.c_str());
		UE_LOG(LogLibCityGMLImporter, Log, TEXT("[%s] %s"), *(LogLevelNames[(int32)level]), *msg);
	}

private:

	TArray<FString>		LogLevelNames;
};


GenericTwin::ICityGMLImporter* FLibCityGMLImporter::Create()
{
	return new FLibCityGMLImporter;
}

FLibCityGMLImporter::FLibCityGMLImporter()
{
}

FLibCityGMLImporter::~FLibCityGMLImporter()
{
}

FString FLibCityGMLImporter::GetImporterName() const
{
	return FString(TEXT("LibFLibCityGMLImporter"));
}


bool FLibCityGMLImporter::Load(const FString &cityGMLFile)
{
	std::shared_ptr<Logger> logger(new Logger);

	UE_LOG(LogLibCityGMLImporter, Log, TEXT("Trying to load %s"), *cityGMLFile);

	citygml::ParserParams params;
	// std::unique_ptr<TesselatorBase> tesselator = std::unique_ptr<TesselatorBase>(new Tesselator(logger) );
	// m_CityModel = citygml::load(TCHAR_TO_ANSI(*cityGMLFile), params, std::move(tesselator), logger);
	m_CityModel = citygml::load(TCHAR_TO_ANSI(*cityGMLFile), params, logger);

	bool res = false;
	if(m_CityModel)
	{
		UE_LOG(LogLibCityGMLImporter, Log, TEXT("Successfully loaded"));

		const citygml::Envelope &cityModelEnv = m_CityModel->getEnvelope();
		if(cityModelEnv.validBounds())
		{
			const TVec3d &lower = cityModelEnv.getLowerBound();
			const TVec3d &upper = cityModelEnv.getUpperBound();
			m_MinExtent = FVector(lower.x, lower.y, lower.z);
			m_MaxExtent = FVector(upper.x, upper.y, upper.z);
		}
		else
		{
			m_MinExtent = FVector(std::numeric_limits<double>::max());
			m_MaxExtent = FVector(std::numeric_limits<double>::min());
			const uint32 numCityRoots = m_CityModel->getNumRootCityObjects();
			for(uint32 i = 0; i < numCityRoots; ++i)
			{
				const citygml::CityObject& cityObj = m_CityModel->getRootCityObject(i);
				const citygml::Envelope &env = cityObj.getEnvelope();

				if(env.validBounds())
				{
					const TVec3d &lower = env.getLowerBound();
					const TVec3d &upper = env.getUpperBound();

					if(lower.x < m_MinExtent.X)	m_MinExtent.X = lower.x;
					if(lower.y < m_MinExtent.Y)	m_MinExtent.Y = lower.y;
					if(lower.z < m_MinExtent.Z)	m_MinExtent.Z = lower.z;
					if(upper.x > m_MaxExtent.X)	m_MaxExtent.X = upper.x;
					if(upper.y > m_MaxExtent.Y)	m_MaxExtent.Y = upper.y;
					if(upper.z > m_MaxExtent.Z)	m_MaxExtent.Z = upper.z;
				}
			}
		}

		res = true;
	}
	else
	{
		UE_LOG(LogLibCityGMLImporter, Log, TEXT("Loading failed"));
	}
	return res;
}

TArray<FString> FLibCityGMLImporter::GetThemes() const
{
	TArray<FString> themes;

	const uint32 numThemes = m_CityModel->getNumThemes();
	for(uint32 i = 0; i < numThemes; ++i)
	 	themes.Add( ANSI_TO_TCHAR(m_CityModel->getTheme(i)) );

	return themes;
}

TArray<GenericTwin::SCityGMLBuilding> FLibCityGMLImporter::GetBuildings(GenericTwin::ICityGMLCoordinateTransform &transform, const FString &Theme, const TSet<FString> &filterList, bool isWhiteList)
{
	TArray<GenericTwin::SCityGMLBuilding> buildings;

	if(m_CityModel)
	{
		const uint32 numCityRoots = m_CityModel->getNumRootCityObjects();
		for(uint32 i = 0; i < numCityRoots; ++i)
		{
			const citygml::CityObject& cityObj = m_CityModel->getRootCityObject(i);
			const citygml::Envelope &env = cityObj.getEnvelope();
			const citygml::CityObject::CityObjectsType type = cityObj.getType();
			
			if	(	type != citygml::CityObject::CityObjectsType::COT_Building
				&&	type != citygml::CityObject::CityObjectsType::COT_GenericCityObject
				)
				continue;

			if	(	cityObj.getGeometriesCount() > 0
				&&	isIncluded( FString(ANSI_TO_TCHAR(cityObj.getId().c_str())), filterList, isWhiteList)
				)
			{
				GenericTwin::SCityGMLBuilding building;
				if(createBuilding(transform, cityObj, TCHAR_TO_ANSI(*Theme), building))
				{
					buildings.Add(building);
				}
			}
			else
			{
				for(uint32 j = 0; j < cityObj.getChildCityObjectsCount(); ++j)
				{
					const citygml::CityObject& childCityObj = cityObj.getChildCityObject(j);
					if	(	childCityObj.getGeometriesCount() > 0
						&&	isIncluded(FString(ANSI_TO_TCHAR(cityObj.getId().c_str())), filterList, isWhiteList)
						)
					{
						GenericTwin::SCityGMLBuilding building;
						if(createBuilding(transform, childCityObj, TCHAR_TO_ANSI(*Theme), building))
						{
							buildings.Add(building);
						}
					}

				}
			}

		}

	}

	return buildings;
}


bool FLibCityGMLImporter::createBuilding(GenericTwin::ICityGMLCoordinateTransform &transform, const citygml::CityObject &cityObj, const char *theme, GenericTwin::SCityGMLBuilding &building)
{
	const uint32 lod = cityObj.getGeometry(0).getLOD();
	for (uint32 i = 0; i < cityObj.getGeometriesCount(); ++i)
	{
		const citygml::Geometry& geometry = cityObj.getGeometry(i);
		if	(	geometry.getLOD() == lod
			&&	geometry.getPolygonsCount() > 0
			)
		{
			if(geometry.getType() == citygml::Geometry::GeometryType::GT_Unknown)
				addSurfaces(transform, geometry, theme, building);
			else
				addSurface(transform, geometry, theme, building);
		}	
		
		for (uint32 j = 0; j < geometry.getGeometriesCount(); ++j)
		{
			const citygml::Geometry& childGeometry = geometry.getGeometry(j);
			if	(	childGeometry.getLOD() == lod
				&&	childGeometry.getPolygonsCount() > 0
				)
			{
				if(childGeometry.getType() == citygml::Geometry::GeometryType::GT_Unknown)
					addSurfaces(transform, childGeometry, theme, building);
				else
					addSurface(transform, childGeometry, theme, building);
			}	
		}
	}
	return building.surfaces.Num() > 0;
}


void FLibCityGMLImporter::addSurfaces(GenericTwin::ICityGMLCoordinateTransform &transform, const citygml::Geometry &geometry, const char *theme, GenericTwin::SCityGMLBuilding &building)
{
	const FString id = FString(geometry.getId().c_str());

	uint32 numPolys = geometry.getPolygonsCount();
	for (uint32 i = 0; i < numPolys; ++i)
	{
		const citygml::Polygon& p = *(geometry.getPolygon(i));
		const std::vector<TVec3d>& vertices = p.getVertices();
		const std::vector<unsigned int>& indices = p.getIndices();

		if (vertices.size() < 3 || indices.size() < 3)
			continue;

		GenericTwin::SCityGMLSurface surface;

		bool hasTexCoords = false;
		if(theme)
		{
			const char *textureUrl = p.getTextureUrlFor(theme);
			if(textureUrl)
			{
				surface.texture_url = ANSI_TO_TCHAR(textureUrl);
				hasTexCoords = true;
			}
		}

		for(size_t vInd = 0; vInd < vertices.size(); ++vInd)
		{
			TVec3d srcPos = vertices[vInd];

			FVector vPos = transform(FVector(srcPos.x, srcPos.y, srcPos.z));

			TVec2f texCoord;
			if	(	hasTexCoords
				&&	p.getTexCoordForTheme(theme, vInd, texCoord)
				)
				surface.vertices.Add(GenericTwin::SCityGMLVertex(vPos, FVector2D(texCoord.x, texCoord.y)));
			else
				surface.vertices.Add(GenericTwin::SCityGMLVertex(vPos));
		}

		for(uint32 n = 0, count = static_cast<uint32> (indices.size()) / 3; n < count; ++n)
		{
			surface.indices.Add(indices[3 * n + 2]);
			surface.indices.Add(indices[3 * n + 1]);
			surface.indices.Add(indices[3 * n]);
		}

		if(surface.indices.Num() > 2)
		{
			surface.surface_id = id;
			surface.surface_type = GenericTwin::SCityGMLSurface::Type::Unknown;
			surface.CalculateNormals();

			building.surfaces.Add(surface);
		}

	}
}

void FLibCityGMLImporter::addSurface(GenericTwin::ICityGMLCoordinateTransform &transform, const citygml::Geometry &geometry, const char *theme, GenericTwin::SCityGMLBuilding &building)
{
	GenericTwin::SCityGMLSurface surface;

	bool hasTexCoords = false;
	// if(theme)
	// {
	// 	const char *textureUrl = p.getTextureUrlFor(theme);
	// 	if(textureUrl)
	// 	{
	// 		surface.texture_url = ANSI_TO_TCHAR(textureUrl);
	// 		hasTexCoords = true;
	// 	}
	// }

	bool isValid = false;
	uint32 numPolys = geometry.getPolygonsCount();
	for (uint32 i = 0; i < numPolys; ++i)
	{
		const citygml::Polygon& p = *(geometry.getPolygon(i));
		const std::vector<TVec3d>& vertices = p.getVertices();
		const std::vector<unsigned int>& indices = p.getIndices();

		if (vertices.size() < 3 || indices.size() < 3)
			continue;


		const uint32 indexOffset = surface.vertices.Num();

		for(size_t vInd = 0; vInd < vertices.size(); ++vInd)
		{
			TVec3d srcPos = vertices[vInd];

			FVector vPos = transform(FVector(srcPos.x, srcPos.y, srcPos.z));

			TVec2f texCoord;
			if	(	hasTexCoords
				&&	p.getTexCoordForTheme(theme, vInd, texCoord)
				)
				surface.vertices.Add(GenericTwin::SCityGMLVertex(vPos, FVector2D(texCoord.x, texCoord.y)));
			else
				surface.vertices.Add(GenericTwin::SCityGMLVertex(vPos));
		}

		// for(uint32 n = 0, count = static_cast<uint32> (indices.size()) / 3; n < count; ++n)
		// {
		// 	surface.indices.Add(indices[3 * n]);
		// 	surface.indices.Add(indices[3 * n + 1]);
		// 	surface.indices.Add(indices[3 * n + 2]);
		// }

		for(auto &ind : indices)
			surface.indices.Add(static_cast<int32>(ind + indexOffset));

		isValid = true;
	}

	if(isValid)
	{
		surface.surface_id = FString(geometry.getId().c_str());
		surface.surface_type = getSurfaceType(geometry);
		surface.CalculateNormals();

		building.surfaces.Add(surface);
	}
}


GenericTwin::SCityGMLSurface::Type FLibCityGMLImporter::getSurfaceType(const citygml::Geometry &geometry)
{
	GenericTwin::SCityGMLSurface::Type type = GenericTwin::SCityGMLSurface::Type::Unknown;

	switch(geometry.getType())
	{
		case citygml::Geometry::GeometryType::GT_Roof:
			type = GenericTwin::SCityGMLSurface::Type::Roof;
			break;

		case citygml::Geometry::GeometryType::GT_Wall:
			type = GenericTwin::SCityGMLSurface::Type::Wall;
			break;

		case citygml::Geometry::GeometryType::GT_Ground:
			type = GenericTwin::SCityGMLSurface::Type::Ground;
			break;

		case citygml::Geometry::GeometryType::GT_Closure:
			type = GenericTwin::SCityGMLSurface::Type::Closure;
			break;

		case citygml::Geometry::GeometryType::GT_Floor:
			type = GenericTwin::SCityGMLSurface::Type::Floor;
			break;

		case citygml::Geometry::GeometryType::GT_InteriorWall:
			type = GenericTwin::SCityGMLSurface::Type::InteriorWall;
			break;

		case citygml::Geometry::GeometryType::GT_Ceiling:
			type = GenericTwin::SCityGMLSurface::Type::Ceiling;
			break;

		case citygml::Geometry::GeometryType::GT_OuterCeiling:
			type = GenericTwin::SCityGMLSurface::Type::OuterCeiling;
			break;

		case citygml::Geometry::GeometryType::GT_OuterFloor:
			type = GenericTwin::SCityGMLSurface::Type::OuterFloor;
			break;

		default:
			break;
	}

	return type;
}
