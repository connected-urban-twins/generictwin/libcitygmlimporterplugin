/**

The MIT License (MIT)

Copyright (c) 2023 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/

using System.IO;
using UnrealBuildTool;

public class LibCityGMLImporterPlugin : ModuleRules
{
	public LibCityGMLImporterPlugin(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
		
		PublicIncludePaths.AddRange(
			new string[] {
				// ... add public include paths required here ...
			}
			);
				
		
		PrivateIncludePaths.AddRange(
			new string[] {
				// ... add other private include paths required here ...
			}
			);
			
		
		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core",
				// ... add other public dependencies that you statically link with here ...
			}
			);
			
		
		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"CoreUObject",
				"Engine",
				"Slate",
				"SlateCore",
				// ... add private dependencies that you statically link with here ...	

				"GenericTwinCore",
				"GenericTwinCityGMLImport",

			}
			);
		
		
		DynamicallyLoadedModuleNames.AddRange(
			new string[]
			{
				// ... add any modules that your module loads dynamically here ...
			}
			);
		
		AddLibCityGML();
	}

	void AddLibCityGML()
	{
		if (Target.Platform == UnrealTargetPlatform.Win64)
		{
			string libCtyGMLPath = Path.Combine(PluginDirectory, "Source", "ThirdParty", "libcitygml");

            PublicIncludePaths.Add(Path.Combine(libCtyGMLPath, "include"));
	
			// Add the import library
			PublicAdditionalLibraries.Add(Path.Combine(libCtyGMLPath, "lib", "citygml.lib"));

			if (Target.bBuildEditor)
			{
				RuntimeDependencies.Add(Path.Combine(PluginDirectory, "../../Binaries/Win64", "citygml.dll"), Path.Combine(PluginDirectory, "Source/ThirdParty/libcitygml/bin/citygml.dll"));
				RuntimeDependencies.Add(Path.Combine(PluginDirectory, "../../Binaries/Win64", "xerces-c_3_2.dll"), Path.Combine(PluginDirectory, "Source/ThirdParty/libcitygml/bin/xerces-c_3_2.dll"));
			}
			else
			{
				RuntimeDependencies.Add("$(TargetOutputDir)/citygml.dll", Path.Combine(PluginDirectory, "Source/ThirdParty/libcitygml/bin/citygml.dll"));
				RuntimeDependencies.Add("$(TargetOutputDir)/xerces-c_3_2.dll", Path.Combine(PluginDirectory, "Source/ThirdParty/libcitygml/bin/xerces-c_3_2.dll"));
			}

        }
	}
}
